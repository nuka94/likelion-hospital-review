package com.example.likelionhospitalreview.service;


import com.example.likelionhospitalreview.domain.dto.UserDto;
import com.example.likelionhospitalreview.domain.dto.UserJoinRequest;
import com.example.likelionhospitalreview.domain.entity.Users;
import com.example.likelionhospitalreview.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    private final BCryptPasswordEncoder encoder;

    public UserDto registerUser(UserJoinRequest userJoinRequest) {
        userRepository.findByUserName(userJoinRequest.getUserName())
                .ifPresent((user) -> {
                    throw new RuntimeException("중복 이름입니다.");
                });

        Users saveUser = userRepository.save(userJoinRequest.toEntity(encoder.encode(userJoinRequest.getPassword())));

        return UserDto.builder()
                .id(saveUser.getId())
                .userName(saveUser.getUserName())
                .email(saveUser.getEmailAddress())
                .grade(saveUser.getGrade())
                .build();

    }

}
