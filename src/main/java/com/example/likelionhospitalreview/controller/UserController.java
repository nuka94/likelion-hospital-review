package com.example.likelionhospitalreview.controller;

import com.example.likelionhospitalreview.domain.dto.UserDto;
import com.example.likelionhospitalreview.domain.dto.UserJoinRequest;
import com.example.likelionhospitalreview.domain.dto.UserJoinResponse;
import com.example.likelionhospitalreview.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
public class UserController {
    private final UserService userService;

    @PostMapping("/join")
    public ResponseEntity<UserJoinResponse> join(@RequestBody UserJoinRequest userJoinRequest) {
        UserDto userDto = userService.registerUser(userJoinRequest);
        log.info("grade : {}", userDto.getGrade());
        UserJoinResponse userJoinResponse = new UserJoinResponse(userDto.getUserName(), userDto.getEmail(), userDto.getGrade());
        return ResponseEntity.ok().body(userJoinResponse);
    }

}
