package com.example.likelionhospitalreview.domain.enums;

public enum UserGrade {
    REGULAR,
    ADMIN
}
